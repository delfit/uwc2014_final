namespace :point do
  desc "Emulate points work"
  task emulate: :environment do
    url = 'http://127.0.0.1:3000/points/commit.json'


    while true
      commit = {
        point_id: Point.offset(rand(Point.count)).first.id,
        type: Transport.offset(rand(Transport.count)).first.id
      }

      commit[:checkin] = []
      rand(1..10).times do
        commit[:checkin] << [
          rand_time(3.months.ago),
          Card.offset(rand(Card.count)).first.id 
        ]
      end

      commit[:checkout] = []
      rand(1..10).times do
        commit[:checkout] << [
          rand_time(3.months.ago),
          Card.offset(rand(Card.count)).first.id 
        ]
      end


      puts "Sending '#{commit.to_json} to #{url}"
      system "curl -i -X POST -H 'Content-Type: application/json' -d '#{commit.to_json}' #{url}"

      sleep 1
    end
  end

  private
    # from http://stackoverflow.com/a/2411246
    def rand_in_range(from, to)
      rand * (to - from) + from
    end

    def rand_time(from, to=Time.now)
      Time.at(rand_in_range(from.to_f, to.to_f))
    end 

end
