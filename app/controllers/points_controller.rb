class PointsController < ApplicationController
  skip_before_filter :verify_authenticity_token, :only => [:commit]

  def commit
    point = Point.find(params[:point_id])

    params[:checkin].each do |card_and_timestamp|
      card = Card.find(card_and_timestamp.last)
      trip = Trip.find_or_create_by(card: card, point_from: point)

      Check.create(
        point: point,
        card: card,
        kind: Check.kinds[:in],
        checked_at: card_and_timestamp.first
      )
      
      trip.is_invalid = true if trip.is_started
      trip.is_invalid = true if trip.is_finished

      trip.is_started = true unless trip.is_invalid
      trip.save
    end

    params[:checkout].each do |card_and_timestamp|
      card = Card.find(card_and_timestamp.last)
      trip = Trip.where(card: card).first

      Check.create(
        point: point,
        card: card,
        kind: Check.kinds[:out],
        checked_at: card_and_timestamp.first
      )

      trip.is_invalid = true unless trip.is_started
      trip.is_invalid = true if trip.is_finished

      trip.is_finished = true unless trip.is_invalid
      trip.save
    end

    respond_to do |format|
      format.json { render nothing: true, status: 200 }
    end
  end
end
