class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def dashboard
    @routes = Route.all;
    @route = Route.new
    @card = Card.new

    if !params[:card]
      params[:card] = {id:Card.last.id}
    end

    if params[:route] && params[:route][:id]
      @trips = Trip.joins(:point_from).where('route_id = ' + params[:route][:id]).invalid.all;
    else
      params[:route] = {id:''}
      params[:route] = {kind:''}
      @trips = Trip.invalid.all
      # @trips = Trip.joins(:point_from, :route).joins.where('route_id = 1').invalid.all;
    end

    @piksData = Route.all

    @cardTrips = Trip.all.where(card_id:params[:card][:id])

    @cart_routes = {
        title: 'New',
        points: []
    };
    # @cardTrips.each.do |trip|

    # end

    @distances = [
      {name: "Некорректное расстояние",data:[[Time.parse("2010-01-01").getutc,2]]}, 
      {name:"Общее расстояние",data:[[Time.parse("2010-01-01").getutc,2]]}
    ];

    @picks = [
      [Time.parse("2010-01-01").getutc,2],
      [Time.parse("2010-01-02").getutc,5]
    ];

    @params = params;
    @transports = Transport.all
  end

  private
    def route_params
      params.require(:route).permit(:id, :kind)
    end

    def cart_params
      params.require(:cart).permit(:id)
    end
end
