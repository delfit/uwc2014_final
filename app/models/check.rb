class Check < ActiveRecord::Base
  belongs_to :card
  belongs_to :point

  enum kind: [:in, :out]
end