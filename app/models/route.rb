class Route < ActiveRecord::Base
  belongs_to :transport

  has_many :points
end
