class Distance < ActiveRecord::Base
  belongs_to :point_from, class_name: Point
  belongs_to :point_to, class_name: Point
end
