class Trip < ActiveRecord::Base
  belongs_to :card
  belongs_to :point_from, class_name: Point
  belongs_to :point_to, class_name: Point

  has_many :from_distances, class_name: Distance, primary_key: :point_from_id, foreign_key: :point_from_id
  has_many :to_distances, class_name: Distance, primary_key: :point_to_id, foreign_key: :point_to_id

  def distances
    from_distances + to_distances
  end

  scope :finished, -> {
    where(is_finished: true)
  }

  scope :invalid, -> {
    where(is_invalid: true)
  }

  scope :started, -> {
    where(is_started: true)
  }

  scope :valid, -> {
    where(is_invalid: false)
  }

  def self.total_distance
    sum = 0
    Trip.all.each do |trip| 
      trip.distances.each do |distance|
        sum += distance.value
      end
    end

    sum
  end

  def self.invalid_distance
    sum = 0
    Trip.all.invalid.each do |trip| 
      trip.distances.each do |distance|
        sum += distance.value
      end
    end

    sum
  end
end
