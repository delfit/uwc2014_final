class Point < ActiveRecord::Base
  belongs_to :route
  
  has_many :checks
end
