class Card < ActiveRecord::Base
  has_many :checks
  has_many :trips
end
