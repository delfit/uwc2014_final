(function($) {
  // map settings
  var mapSettings;
  var gMap;
  var gMarkers = [];
  var gRoute;
  var directionsDisplay;
  var directionsService;
  var geocoder;

  var methods = {
    init: function( options ) {
      $this = $(this);

      mapSettings = $.extend({
        'navigator': false,
        'mapTypeId': google.maps.MapTypeId.ROADMAP,
        'zoom': 6,
        'moveToMarker': false,
        'city': 'Kiev',
        'disableDefaultUI': true,
        'panControl': false,
        'zoomControl': false,
        'mapTypeControl': false,
        'scaleControl': false,
        'streetViewControl': false,
        'overviewMapControl': false
      }, options );

      google.maps.event.addDomListener( window, 'load', function() {
        methods.initMap();
      });
    },

    setRoute: function( options, callback ) {
      if( !directionsDisplay ) {
        return;
      }

      directionsDisplay.setMap(null);

      if( !options || !options.points ) {
        return;
      }

      var points = options.points;
      var wayStart, wayEnd;
      var wayPoints = [];


      // сформировать список контрольных точек
      var countPoints = points.length;
      for( var pointIndex = 0; pointIndex < countPoints; pointIndex++ ) {
        if( pointIndex == 0 ) {
          wayStart = points[ pointIndex ];
        }

        if( pointIndex == ( countPoints - 1 ) ) {
          wayEnd = points[ pointIndex ];
        }

        wayPoints.push({
          location: points[ pointIndex ],
          stopover: true
        });
      }

      var request = {
          origin: wayStart,
          destination: wayEnd,
          waypoints: wayPoints,
          optimizeWaypoints: true,
          travelMode: google.maps.TravelMode.DRIVING
      };

      directionsService.route( request, function(response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
          directionsDisplay.setMap(gMap);
          directionsDisplay.setDirections(response);
          var route = response.routes[0];

          if( callback ) {
            callback( route );
          }

          gRoute = route;
        }
      });
    },

    setMarkers: function( markers ) {
      methods.clearMarkers();
      var countMarkers = markers.length;
      for( var i = 0; i < countMarkers; i ++ ) {
        var currentMarker = markers[i];
        methods.addMarker( currentMarker );
      }
    },

    addMarker: function( options ) {
      if( !options.latitude && !options.longitude && options.address ) {
        // console.log( options );
        methods.addressToCoordinates( options.address, function( location ) {
          options.longitude = location.A;
          options.latitude = location.k;
          methods.addMarker( options );
        });

        return;
      }

      var markerPosition =  new google.maps.LatLng(
        options.latitude,
        options.longitude
      );

      var markerOptions = $.extend({
        // 'animation': google.maps.Animation.BOUNCE,
        'position': markerPosition,
        'map': gMap
      }, options );

      delete markerOptions.latitude;
      delete markerOptions.longitude;

      var marker = new google.maps.Marker( markerOptions );

      // info window for marker
      if( options.content || options.url ) {
        var markerInfoWindow = new google.maps.InfoWindow({
            content: options.content
        });

        google.maps.event.addListener( marker, 'click', function() {
          if( options.url ) {
            window.location = options.url;
          }

          markerInfoWindow.open( gMap, marker );

          /*** assing orders logic ***/
            var markerPosition = marker.position;

            var $assignCont = $( '[data-cont="assign"]' );
            var $assingActionBtn = $assignCont.find( '[data-action="assign"]' );

            var currentPosition = markerPosition;
            var destinationPosition = markerPosition;

            if( gRoute ) {
              if( $assingActionBtn.length > 0 ) {
                var routeFirstPoint = $( gRoute.legs ).first();
                var destinationPosition = routeFirstPoint[0].start_location;
              }
              else {
                var routeLastPoint = $( gRoute.legs ).last();
                var destinationPosition = routeLastPoint[0].end_location;
              }
            }

            // fill dunamic attributes when assign active
            methods.getDistanceBeetwen( currentPosition, destinationPosition, function( route ) {
              var durationText = '';
              var distanceText = '';
              var startAddressText = '';

              if( route.legs && route.legs[0] ) {
                distanceText = route.legs[0].distance.text;
                durationText = route.legs[0].duration.text;
                startAddressText = route.legs[0].start_address;
              }

              $assignCont.find( '[data-cont="relative-distance"]' ).text( distanceText );
              $assignCont.find( '[data-cont="estimate-duration"]' ).text( durationText );
              $assignCont.find( '[data-cont="current-address"]' ).text( startAddressText );
            });

            $assingActionBtn.off().click( function() {
              var $activeItem = $( '[data-id][data-active="1"]' );
              var activeItemId = $activeItem.attr( 'data-id' );
              var currentId = $(this).parents( '[data-assign-id]' ).attr( 'data-assign-id' );

              $.ajax({
                method: 'POST',
                url: $(this).attr( 'href' ),
                data: {
                  'order_id': activeItemId,
                  'car_id': currentId
                },
                success: function( data ) {
                  if( data.success ) {
                    // disable cars for this item
                    var $currentItem = $( '[data-id="' + activeItemId + '"]' );
                    $currentItem.find( '[data-cont="item-status"]' ).text( data.status );

                    // refresh cars on the map
                    methods.setMarkers( data.car_markers );
                  }
                },
                complete: function() {}
              });


              return false;
            });
          /*** assing orders logic ***/
        });

        delete markerOptions.content;
      }

      gMarkers.push( marker );

      if( mapSettings.moveToMarker ) {
        gMap.setCenter( markerPosition );
      }
    },

    showMarkers: function() {
      for (var i = 0; i < gMarkers.length; i++) {
        gMarkers[i].setMap( gMap );
      }
    },

    hideMarkers: function() {
      for (var i = 0; i < gMarkers.length; i++) {
        gMarkers[i].setMap( null );
      }
    },

    clearMarkers: function() {
      methods.hideMarkers();
      gMarkers = [];
    },

    initMap: function() {
      var mapOptions = $.extend({
        center: new google.maps.LatLng(-34.397, 150.644),
        zoom: mapSettings.zoom,
        mapTypeId: mapSettings.mapTypeId
      }, mapSettings );

      gMap = new google.maps.Map( $( '#' + mapSettings.id )[0], mapOptions );

      directionsService = new google.maps.DirectionsService();

      directionsDisplay = new google.maps.DirectionsRenderer({suppressMarkers: true});
      directionsDisplay.setMap( gMap );

      geocoder = new google.maps.Geocoder();

      // перейти к начальной позиции на карте
      methods.setInitialLocation();

      // выставить начальные маркеры
      if( mapSettings.markers ) methods.setMarkers( mapSettings.markers );
      if( mapSettings.route ) methods.setRoute( mapSettings.route );
    },

    setInitialLocation: function() {
      if( navigator.geolocation && mapSettings.navigator ) {
        navigator.geolocation.getCurrentPosition(
          function( position ) {
            var newPosition = new google.maps.LatLng(
              position.coords.latitude,
              position.coords.longitude
            );

            gMap.setCenter( newPosition );
          },
          function() {
            methods.setLocationToCity( mapSettings.city );
          }
        );
      }
      else {
         methods.setLocationToCity( mapSettings.city );
      }
    },

    addressToCoordinates: function( address, callback ) {
      var geocoder = new google.maps.Geocoder();
      geocoder.geocode(
        {
          'address': address
        },
        function( results, status ) {
          callback( results[0]['geometry']['location'] );
        }
      );
    },

    setLocationToCity: function( city ) {
      if(!city) {
        return;
      }

      var geocoder = new google.maps.Geocoder();

      geocoder.geocode(
        {
          'address': city,
          'region': 'ua'
        },
        function( results, status ) {
           gMap.setCenter( results[0]['geometry']['location'] );
        }
      );
    },

    getDistanceBeetwen: function( start, end, callback ) {
      console.log( 'getDistanceBeetwen' );
      var request = {
          origin: start,
          destination: end,
          travelMode: google.maps.TravelMode.DRIVING
      };

      directionsService.route( request, function(response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
          var route = response.routes[0];
          callback( route );
        }
      });
    },

    initAutocomplete: function( $field, callback ) {
      var autocomplete = new google.maps.places.Autocomplete( $field[0], {
        componentRestrictions: {
          country: mapSettings.country
        }
      });

      autocomplete.bindTo( 'bounds', gMap );
      google.maps.event.addListener( autocomplete, 'place_changed', function() {
        var place = autocomplete.getPlace();

        if (!place || !place.geometry) {
          return;
        }

        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {
          gMap.fitBounds(place.geometry.viewport);
        } else {
          gMap.setCenter(place.geometry.location);
          gMap.setZoom(17);  // Why 17? Because it looks good.
        }

        callback( $field, place.geometry );
      });
    },

    getCurrentGeolocation: function( callback ) {
      if( navigator.geolocation ) {
        navigator.geolocation.getCurrentPosition(
          function( position ) {
            var newPosition = new google.maps.LatLng(
              position.coords.latitude,
              position.coords.longitude
            );

            geocoder.geocode( { 'latLng': newPosition}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                  callback( results );
                }
            });

          },
          function() {
          }
        );
      }
    }
  }

  $.fn.gmap = function( method ) {
    if ( methods[method] ) {
      return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
    } else if ( typeof method === 'object' || ! method ) {
      return methods.init.apply( this, arguments );
    } else {
      $.error( 'Метод с именем ' +  method + ' не существует для jQuery.gMap' );
    }
  }
})(jQuery);
