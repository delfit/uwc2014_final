// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require foundation
//= require highcharts
//= require turbolinks
//= require_tree .

$(function(){ 
	$(document).foundation(); 

    $('#container').highcharts({
        chart: {
            type: 'pie'
        },
        title: false,
        // xAxis: {
        //     categories: ['Apples', 'Bananas', 'Oranges']
        // },
        // yAxis: {
        //     title: {
        //         text: 'Fruit eaten'
        //     }
        // },
        series: [{
            type: 'pie',
            name: '',
            data: [
                {
                    name: 'Некорректные поездки',
                    y: 30,
                    sliced: false,
                    color:'red'
                },
                {
                    name: 'Корректные поездки',
                    y: 70,
                    sliced: false,
                    color:'green'
                },
            ]
        }]
    });

    $('#peak').highcharts({
        chart: {
            type: 'spline'
        },
        // title: {
        //     text: 'Пиковая загруженность транспорта'
        // },
        title: false,
        subtitle: false,
        xAxis: {
            type: 'datetime',
            dateTimeLabelFormats: { // don't display the dummy year
                month: '%e. %b',
                year: '%b'
            },
            title: {
                text: 'Дата'
            }
        },
        yAxis: {
            title: {
                text: 'Количество поездок'
            },
            min: 0
        },
        tooltip: {
            headerFormat: '<b>{series.name}</b><br>',
            pointFormat: '{point.x:%e. %b}: {point.y}'
        },

        series: [{
            name: 'Метро',
            data: window.picks
        }]
    });
	
	$('#distances').highcharts({
        chart: {
            type: 'column'
        },
        title: false,
        subtitle: false,
        xAxis: {
            type: 'datetime',
            dateTimeLabelFormats: { // don't display the dummy year
                month: '%e. %b',
                year: '%b'
            },
            title: {
                text: 'Дата'
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Количесво поездок'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: window.distances
    });

	$('#map').gmap({'id':'map', 'route': window.cart_routes});
});
