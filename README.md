UWC 2014 Final
==============


### Установка

```
bundle install
rake db:setup
```


## Эмулятор точки (турникета)

Запускается командой : 

```
rake point:emulate
```