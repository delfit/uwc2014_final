# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


train = Transport.create(title: 'Поезд', slug: 'train')
metro = Transport.create(title: 'Метро', slug: 'metro')
tram = Transport.create(title: 'Трамвай', slug: 'tram')
trolley = Transport.create(title: 'Троллейбус', slug: 'trolley')
bus = Transport.create(title: 'Автобус', slug: 'bus')




# Legend

# route 1 : vinnitsa-kiev, 1-7, metro
# route 2 : kirovograd-dnepropetrovsk, 8-2, metro
# route 3 : summy-uzhgorod, 17-19, tram
# route 4 : zhitomer-simpheropol, 4-16, bus
# route 5 : vinnitsa-simpheropol, 1-16, bus
# route 6 : chernigov-khmelnitskiy, 25-22, train
# route 7 : chernigov-khmelnitskiy, 25-22, bus
# route 8 : vinnitsa-kiev, 1-7, trolley
# route 9 : summy-uzhgorod, 17-19, train
# route 10 : summy-uzhgorod, 17-19, bus

# ===

# route 1 : vinnitsa-kiev, 1-7

vinnitsa_kiev = Route.create(
  title: "vinnitsa-kiev",
  transport: metro
)

vinnitsa = Point.create(
  route: vinnitsa_kiev,
  title: "vinnitsa",
  index: 1,
  lat: 50.21, 
  lon: 30.57
)

kiev = Point.create(
  route: vinnitsa_kiev,
  title: "kiev",
  index: 2,
  lat: 48.55, 
  lon: 24.43
)

Distance.create(
  point_from: vinnitsa, 
  point_to: kiev,
  value: 256
)

# route 2 : kirovograd-dnepropetrovsk, 8-2

kirovograd_dnepropetrovsk = Route.create(
  title: "kirovograd-dnepropetrovsk",
  transport: metro
)

kirovograd = Point.create(
  route: kirovograd_dnepropetrovsk,
  title: "kirovograd",
  index: 1,
  lat: 45.22, 
  lon: 36.27
)

dnepropetrovsk = Point.create(
  route: kirovograd_dnepropetrovsk,
  title: "dnepropetrovsk",
  index: 2,
  lat: 49.14, 
  lon: 28.29
)

Distance.create(
  point_from: kirovograd, 
  point_to: dnepropetrovsk,
  value: 294
)

# route 3 : summy-uzhgorod, 17-19

summy_uzhgorod = Route.create(
  title: "summy-uzhgorod",
  transport: tram
)

zhitomer = Point.create(
  route: summy_uzhgorod,
  title: "zhitomer",
  index: 3,
  lat: 48, 
  lon: 37.48
)

kiev = Point.create(
  route: summy_uzhgorod,
  title: "kiev",
  index: 2,
  lat: 48.55, 
  lon: 24.43
)

lvov = Point.create(
  route: summy_uzhgorod,
  title: "lvov",
  index: 5,
  lat: 50.44, 
  lon: 25.2
)

rovno = Point.create(
  route: summy_uzhgorod,
  title: "rovno",
  index: 4,
  lat: 49.35, 
  lon: 34.34
)

summy = Point.create(
  route: summy_uzhgorod,
  title: "summy",
  index: 1,
  lat: 44.36, 
  lon: 33.32
)

uzhgorod = Point.create(
  route: summy_uzhgorod,
  title: "uzhgorod",
  index: 6,
  lat: 50.55, 
  lon: 34.45
)

Distance.create(
  point_from: summy, 
  point_to: kiev,
  value: 346
)

Distance.create(
  point_from: kiev, 
  point_to: zhitomer,
  value: 131
)

Distance.create(
  point_from: zhitomer, 
  point_to: rovno,
  value: 187
)

Distance.create(
  point_from: rovno, 
  point_to: lvov,
  value: 232
)

Distance.create(
  point_from: lvov, 
  point_to: uzhgorod,
  value: 261
)

# route 4 : zhitomer-simpheropol, 4-16

zhitomer_simpheropol = Route.create(
  title: "zhitomer-simpheropol",
  transport: bus
)

zhitomer = Point.create(
  route: zhitomer_simpheropol,
  title: "zhitomer",
  index: 1,
  lat: 48, 
  lon: 37.48
)

ivano_frankovsk = Point.create(
  route: zhitomer_simpheropol,
  title: "ivano_frankovsk",
  index: 3,
  lat: 47.5, 
  lon: 35.1
)

nikolayev = Point.create(
  route: zhitomer_simpheropol,
  title: "nikolayev",
  index: 6,
  lat: 49.5, 
  lon: 24
)

odessa = Point.create(
  route: zhitomer_simpheropol,
  title: "odessa",
  index: 5,
  lat: 46.58, 
  lon: 32
)

simpheropol = Point.create(
  route: zhitomer_simpheropol,
  title: "simpheropol",
  index: 8,
  lat: 50.37, 
  lon: 26.15
)

ternopol = Point.create(
  route: zhitomer_simpheropol,
  title: "ternopol",
  index: 2,
  lat: 44.57, 
  lon: 34.06
)

kherson = Point.create(
  route: zhitomer_simpheropol,
  title: "kherson",
  index: 7,
  lat: 48.37, 
  lon: 22.18
)

chernovtsy = Point.create(
  route: zhitomer_simpheropol,
  title: "chernovtsy",
  index: 4,
  lat: 51.3, 
  lon: 31.18
)

Distance.create(
  point_from: zhitomer, 
  point_to: ternopol,
  value: 298
)

Distance.create(
  point_from: ternopol, 
  point_to: ivano_frankovsk,
  value: 134
)

Distance.create(
  point_from: ivano_frankovsk, 
  point_to: chernovtsy,
  value: 143
)

Distance.create(
  point_from: chernovtsy, 
  point_to: odessa,
  value: 515
)

Distance.create(
  point_from: odessa, 
  point_to: nikolayev,
  value: 120
)

Distance.create(
  point_from: nikolayev, 
  point_to: kherson,
  value: 51
)

Distance.create(
  point_from: kherson, 
  point_to: simpheropol,
  value: 221
)

# route 5 : vinnitsa-simpheropol, 1-16

vinnitsa_simpheropol = Route.create(
  title: "vinnitsa-simpheropol",
  transport: bus
)

vinnitsa = Point.create(
  route: vinnitsa_simpheropol,
  title: "vinnitsa",
  index: 1,
  lat: 50.21, 
  lon: 30.57
)

nikolayev = Point.create(
  route: vinnitsa_simpheropol,
  title: "nikolayev",
  index: 2,
  lat: 49.5, 
  lon: 24
)

odessa = Point.create(
  route: vinnitsa_simpheropol,
  title: "odessa",
  index: 3,
  lat: 46.58, 
  lon: 32
)

simpheropol = Point.create(
  route: vinnitsa_simpheropol,
  title: "simpheropol",
  index: 4,
  lat: 50.37, 
  lon: 26.15
)

Distance.create(
  point_from: vinnitsa, 
  point_to: nikolayev,
  value: 471
)

Distance.create(
  point_from: nikolayev, 
  point_to: odessa,
  value: 120
)

Distance.create(
  point_from: odessa, 
  point_to: simpheropol,
  value: 392
)

# route 6 : chernigov-khmelnitskiy, 25-22

cherbigov_khmelnitskiy = Route.create(
  title: "chernigov-khmelnitskiy",
  transport: train
)

lutsk = Point.create(
  route: cherbigov_khmelnitskiy,
  title: "lutsk",
  index: 2,
  lat: 48.3, 
  lon: 32.18
)

lvov = Point.create(
  route: cherbigov_khmelnitskiy,
  title: "lvov",
  index: 3,
  lat: 50.44, 
  lon: 25.2
)

ternopol = Point.create(
  route: cherbigov_khmelnitskiy,
  title: "ternopol",
  index: 4,
  lat: 44.57, 
  lon: 34.06
)

khmelnitskiy = Point.create(
  route: cherbigov_khmelnitskiy,
  title: "khmelnitskiy",
  index: 5,
  lat: 50.0, 
  lon: 36.15
)

chernigov = Point.create(
  route: cherbigov_khmelnitskiy,
  title: "chernigov",
  index: 1,
  lat: 48.18, 
  lon: 25.56
)

Distance.create(
  point_from: chernigov, 
  point_to: lutsk,
  value: 949
)

Distance.create(
  point_from: lutsk, 
  point_to: lvov,
  value: 152
)

Distance.create(
  point_from: lvov, 
  point_to: ternopol,
  value: 128
)

Distance.create(
  point_from: ternopol, 
  point_to: khmelnitskiy,
  value: 112
)

# route 7 : chernigov-khmelnitskiy, 25-22, bus

cherbigov_khmelnitskiy = Route.create(
  title: "chernigov-khmelnitskiy",
  transport: bus
)

# route 8 : vinnitsa-kiev, 1-7, trolley

vinnitsa_kiev = Route.create(
  title: "vinnitsa-kiev",
  transport: trolley
)

# route 9 : summy-uzhgorod, 17-19, train

summy_uzhgorod = Route.create(
  title: "summy-uzhgorod",
  transport: train
)

# route 10 : summy-uzhgorod, 17-19, bus

summy_uzhgorod = Route.create(
  title: "summy-uzhgorod",
  transport: bus
)




# сгенерировать поезки
rand(1..20).times do
  Card.create()
end


# сгенерировать поездки
Route.all.each do |route|
  rand(1..20).times do
    points = route.points
    points_count = points.count

    if points_count > 0
      Trip.create(
        card: Card.offset(rand(Card.count)).first,
        point_from: points.offset(rand(points.count)).first,
        point_to: points.offset(rand(points.count)).first
      )
    end
  end
end