class CreateChecks < ActiveRecord::Migration
  def change
    create_table :checks do |t|
      t.references :card, index: true, null: false
      t.references :point, index: true, null: false
      t.integer :kind, null: false
      t.timestamp :checked_at, null: false

      t.timestamps
    end
  end
end
