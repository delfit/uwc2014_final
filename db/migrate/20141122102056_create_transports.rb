class CreateTransports < ActiveRecord::Migration
  def change
    create_table :transports do |t|
      t.string :title, null: false
      t.string :slug, null: false

      t.timestamps
    end
  end
end
