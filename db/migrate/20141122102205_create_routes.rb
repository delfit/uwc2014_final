class CreateRoutes < ActiveRecord::Migration
  def change
    create_table :routes do |t|
      t.string :title, null: false
      t.references :transport, null: false

      t.timestamps
    end
  end
end
