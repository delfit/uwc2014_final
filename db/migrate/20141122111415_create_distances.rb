class CreateDistances < ActiveRecord::Migration
  def change
    create_table :distances do |t|
      t.references :point_from, index: true, null: false
      t.references :point_to, index: true, null: false
      t.decimal :value, null: false

      t.timestamps
    end
  end
end
