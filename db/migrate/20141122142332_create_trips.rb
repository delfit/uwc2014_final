class CreateTrips < ActiveRecord::Migration
  def change
    create_table :trips do |t|
      t.references :card, index: true, null: false
      t.references :point_from, index: true, null: false
      t.references :point_to, index: true
      t.boolean :is_started, default: false, null: false
      t.boolean :is_finished, default: false, null: false
      t.boolean :is_invalid, default: false, null: false

      t.timestamps
    end
  end
end
