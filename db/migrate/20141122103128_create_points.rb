class CreatePoints < ActiveRecord::Migration
  def change
    create_table :points do |t|
      t.references :route, index: true, null: false
      t.string :title, null: false
      t.integer :index, null: false
      t.decimal :lat, null: false
      t.decimal :lon, null: false

      t.timestamps
    end
  end
end
