# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141122142332) do

  create_table "cards", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "checks", force: true do |t|
    t.integer  "card_id",    null: false
    t.integer  "point_id",   null: false
    t.integer  "kind",       null: false
    t.datetime "checked_at", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "checks", ["card_id"], name: "index_checks_on_card_id"
  add_index "checks", ["point_id"], name: "index_checks_on_point_id"

  create_table "distances", force: true do |t|
    t.integer  "point_from_id", null: false
    t.integer  "point_to_id",   null: false
    t.decimal  "value",         null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "distances", ["point_from_id"], name: "index_distances_on_point_from_id"
  add_index "distances", ["point_to_id"], name: "index_distances_on_point_to_id"

  create_table "points", force: true do |t|
    t.integer  "route_id",   null: false
    t.string   "title",      null: false
    t.integer  "index",      null: false
    t.decimal  "lat",        null: false
    t.decimal  "lon",        null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "points", ["route_id"], name: "index_points_on_route_id"

  create_table "routes", force: true do |t|
    t.string   "title",        null: false
    t.integer  "transport_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "transports", force: true do |t|
    t.string   "title",      null: false
    t.string   "slug",       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trips", force: true do |t|
    t.integer  "card_id",                       null: false
    t.integer  "point_from_id",                 null: false
    t.integer  "point_to_id"
    t.boolean  "is_started",    default: false, null: false
    t.boolean  "is_finished",   default: false, null: false
    t.boolean  "is_invalid",    default: false, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "trips", ["card_id"], name: "index_trips_on_card_id"
  add_index "trips", ["point_from_id"], name: "index_trips_on_point_from_id"
  add_index "trips", ["point_to_id"], name: "index_trips_on_point_to_id"

end
